// ==UserScript==
// @name         Old ProtonMail
// @version      1.0
// @description  Automatically use ProtonMail's old interface.
// @author       openpaige
// @namespace    https://gitlab.com/openpaige-user-scripts
// @icon         https://gitlab.com/openpaige-user-scripts/old-protonmail/-/raw/main/icon.png
// @match        *://mail.protonmail.com/*
// @run-at       document-start
// ==/UserScript==

function matchURL(url) {
    return !!url.match(/^(http(s?):\/\/)(mail.protonmail.com)/gim);
}

function rewriteURL(url) {
    return 'https://old.protonmail.com/login' + url.split('protonmail.com').pop();
}

url = window.location.href;
if ( matchURL(url) ) {
    window.location.assign(rewriteURL(url));
}
